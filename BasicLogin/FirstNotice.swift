import Foundation
import UIKit
import RxSwift
import RxCocoa


protocol LoginViewModelInputs {
	func viewDidLoad()
	func usernameChanged(_ username: String?)
	func passwordChanged(_ password: String?)
	func usernameTextFieldDoneEditing()
}

protocol LoginViewModelOutputs {
	var signInEnabled: Observable<Bool> { get }
	var usernameText: Observable<String> { get }
	var passwordText: Observable<String> { get }
	var usernameFieldBecomesFirstResponder: Observable<()> { get }
}

internal protocol LoginViewModelType {
	var inputs: LoginViewModelInputs { get }
	var outputs: LoginViewModelOutputs { get }
}

internal final class LoginViewModel: LoginViewModelType, LoginViewModelInputs, LoginViewModelOutputs {

	internal var inputs: LoginViewModelInputs { return self }
	internal var outputs: LoginViewModelOutputs { return self }
	
	init() {
		usernameFieldBecomesFirstResponder = usernameTextFieldDoneEditingSubject.asObservable()
		usernameText = usernameChangedSubject.asObservable().filter { $0 != nil }.map { $0! }
		passwordText = passwordChangedSubject.asObserver().filter { $0 != nil }.map { $0! }
		signInEnabled = signInEnabledSubject.asObserver()
	}
	
	public let signInEnabled: Observable<Bool>
	public let usernameText: Observable<String>
	public let passwordText: Observable<String>
	public let usernameFieldBecomesFirstResponder: Observable<()>
	
	fileprivate let viewDidLoadSubject = PublishSubject<()>()
	public func viewDidLoad() {
		self.viewDidLoadSubject.onNext(())
	}
	
	fileprivate let signInEnabledSubject = BehaviorSubject<Bool>(value: false)
	public func signInEnabledChanged(_ isEnabled: Bool) {
		self.signInEnabledSubject.onNext(isEnabled)
	}
	
	fileprivate let usernameChangedSubject = BehaviorSubject<String?>(value: nil)
	public func usernameChanged(_ username: String?) {
	  self.usernameChangedSubject.onNext(username)
	}
	fileprivate let passwordChangedSubject = BehaviorSubject<String?>(value: nil)
	public func passwordChanged(_ password: String?) {
	  self.passwordChangedSubject.onNext(password)
	}
	fileprivate let usernameTextFieldDoneEditingSubject = PublishSubject<()>()
	public func usernameTextFieldDoneEditing() {
	  self.usernameTextFieldDoneEditingSubject.onNext(())
	}
}




class LoginView: UIView {
	@IBOutlet weak var UsernameTextField: UITextField!
	@IBOutlet weak var SignInButton: UIButton!
	@IBOutlet weak var PasswordTextField: UITextField!
	@IBOutlet weak var FingerprintButton: UIButton!
	@IBOutlet weak var RememberSwitch: UISwitch!
	
	public let viewmodel = LoginViewModel()
	public let disposeBag = DisposeBag()
	private var ExecuteSignIn = #selector(onSignIn)
	var OnSignInAction: () -> () = {}
	public var OnSignIn: () -> () {
		get {
			return OnSignInAction
		}
		set {
			OnSignInAction = newValue
		}
	}
	
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
	
	override func awakeFromNib() {
		
		FingerprintButton.isHidden = true
		SignInButton.isEnabled = false
		SignInButton.addTarget(self, action: #selector(onSignIn), for: .touchUpInside)
		
		let observable = Observable.combineLatest(
			UsernameTextField.rx.controlEvent([.editingChanged]).asObservable(),
			PasswordTextField.rx.controlEvent([.editingChanged]).asObservable())
		observable.subscribe(onNext: { [weak self] _ in
			guard let self = self else {
				return
			}
			guard let usernameText = self.UsernameTextField.text else { return }
			guard let passwordText = self.PasswordTextField.text else { return }
			self.viewmodel.passwordChanged(passwordText)
			let shouldEnable = usernameText.count>3 && passwordText.count>3
			self.viewmodel.signInEnabledChanged(shouldEnable)
		}).disposed(by: self.disposeBag)
	}
	
	@objc func onSignIn() {
		print("Sign In Request")
		OnSignInAction()
	}
}

class LoginViewController: UIViewController {
	private var loginView: LoginView?
	
	override func viewDidLoad() {
		print("Loading Login View Controller")
		loginView = Bundle.main.loadNibNamed("iPhoneLoginView", owner: self, options: nil)![0] as? LoginView
		loginView?.OnSignIn = onDisplayAlert
		bindViewModel()
		loginView?.viewmodel.viewDidLoad()
	}
	func bindViewModel() {
		guard let bag = loginView?.disposeBag else { return }
		guard let viewmodel = loginView?.viewmodel else { return }
		
		_ = viewmodel.signInEnabled.subscribe(onNext : { isEnabled in
			self.loginView?.SignInButton?.isEnabled = isEnabled
		}).disposed(by: bag)
		
	}
	func onDisplayAlert() {
		let alert = UIAlertController(title: "Alert", message: "Sign In Request", preferredStyle: UIAlertController.Style.alert)
		alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: { _ in
			print("Cancel")
		}))
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
			print("Ok")
		}))

		self.present(alert, animated: true, completion: nil)
	}
}

class DashboardViewController: UIViewController {
	lazy var tableView: UITableView = {
	  let tbl = UITableView()
	  return tbl
	}()
	
	override func viewDidLoad() {
		tableView.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(tableView)
		tableView.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
		tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
		tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
		tableView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
	}
}
