//
//  AppDelegate.swift
//  BasicLogin
//
//  Created by Rob Zmudzinski on 1/15/20.
//  Copyright © 2020 Rob Zmudzinski. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
				// Override point for customization after application launch.
		self.window = UIWindow(frame: UIScreen.main.bounds)

		let initialViewController = LoginViewController() //ViewController()
		//initialViewController.view.backgroundColor = UIColor.blue

		self.window?.rootViewController = UINavigationController(rootViewController: initialViewController)
		self.window?.makeKeyAndVisible()

		return true
	}

}

